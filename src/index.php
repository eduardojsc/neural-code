<!DOCTYPE html>
<html lang="pt-br">
   <head>
      
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <link rel="shortcut icon" href="assets/img/favicon/favicon.ico" type="image/x-icon">
      <title>Neural Code - De um dev para dev</title>

      <!-- FONTE SITE-->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200&display=swap" rel="stylesheet">   

      <!-- TAILWIND CSS -->
      <link rel="stylesheet" href="assets/css/tailwind-style.css">

      <!-- HIGHLIGHT JS -->         
      <link rel="stylesheet" href="assets/js/highlight/styles/default.css">
      <script src="assets/js/highlight/highlight.pack.js"></script>
      <script>hljs.highlightAll();</script>
      
      <!-- ALPINE.JS -->
      <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

   </head>
   <body class="p-5 text-white bg-default-dark font-inter">
      
      <header>
         <?php include_once 'assets/includes/header.php' ?>
      </header>
         
      <main>

         <section class="grid mt-10 lg:grid-cols-layout-3-cols-fr">

            <nav class="hidden lg:block">
               <?php include 'assets/includes/menu.php' ?>
            </nav>

            <div class="grid col-span-2 gap-5 md:grid-cols-2">
               <!-- CONTEUDO SITE (CRIAR UM FOR PARA REPLICAR) -->
               <div class="bg-indigo-900 shadow-md rounded-t-md rounded-b-md">
                  <div class="p-5 bg-indigo-700 rounded-t-md">
                     <div class="bg-black rounded-md shadow-xl">
                        <div class="flex gap-2 pt-4 pl-4"> 
                           <img src="assets/img/icons/Ellipse 1.svg" alt="">
                           <img src="assets/img/icons/Ellipse 2.svg" alt="">
                           <img src="assets/img/icons/Ellipse 3.svg" alt="">
                        </div>
                        <div class="mt-3 ml-2 javascript">                     
                           <pre class="whitespace-normal">
                              <code >
                                 const pluckDeep = key => obj => key.split('.').reduce((accum, key) => accum[key], obj)
                                 
                                 const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)
                                 
                                 const unfold = (f, seed) => {
                                    const go = (f, seed, acc) => {
                                       const res = f(seed)
                                       return res ? go(f, res[1], acc.concat([res[0]])) : acc
                                    }
                                    return go(f, seed, [])
                                 }
                              </code>
                           </pre>
                        </div>
                     </div>
                  </div>
                  <div class="p-5 text-white">
                     <h1 class="text-2xl font-bold">Título do projeto</h1>
                     <h2 class="py-5">Essa é a descrição do meu projeto.</h2>

                     <div class="grid grid-cols-2">
                        <div class="flex items-center gap-4">
                           <div class="flex gap-2">
                              <img src="assets/img/icons/comments.svg" alt="">
                              <p>13</p>
                           </div>

                           <div class="flex gap-2">
                              <img src="assets/img/icons/heart.svg" alt="">
                              <p>13</p>
                           </div>
                        </div>

                        <div class="flex items-center justify-end gap-2">
                           <img class="w-8 rounded-full" src="assets/img/usuario/00100sPORTRAIT_00100_BURST20200711152721154_COVER-01.jpg" alt="">
                           <p>@eduardojsc</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bg-pink-900 shadow-md rounded-t-md rounded-b-md">
                  <div class="p-5 bg-pink-400 rounded-t-md">
                     <div class="bg-black rounded-md shadow-xl">
                        <div class="flex gap-2 pt-4 pl-4">
                           <img src="assets/img/icons/Ellipse 1.svg" alt="">
                           <img src="assets/img/icons/Ellipse 2.svg" alt="">
                           <img src="assets/img/icons/Ellipse 3.svg" alt="">
                        </div>
                        <div class="mt-3 ml-2 javascript">
                           <pre class="whitespace-normal">
                                          <code >
                                             const pluckDeep = key => obj => key.split('.').reduce((accum, key) => accum[key], obj)
                                             
                                             const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)
                                             
                                             const unfold = (f, seed) => {
                                                const go = (f, seed, acc) => {
                                                   const res = f(seed)
                                                   return res ? go(f, res[1], acc.concat([res[0]])) : acc
                                                }
                                                return go(f, seed, [])
                                             }
                                          </code>
                                       </pre>
                        </div>
                     </div>
                  </div>
                  <div class="p-5 text-white">
                     <h1 class="text-2xl font-bold">Título do projeto</h1>
                     <h2 class="py-5">Essa é a descrição do meu projeto.</h2>
               
                     <div class="grid grid-cols-2">
                        <div class="flex items-center gap-4">
                           <div class="flex gap-2">
                              <img src="assets/img/icons/comments.svg" alt="">
                              <p>13</p>
                           </div>
               
                           <div class="flex gap-2">
                              <img src="assets/img/icons/heart.svg" alt="">
                              <p>13</p>
                           </div>
                        </div>
               
                        <div class="flex items-center justify-end gap-2">
                           <img class="w-8 rounded-full"
                              src="assets/img/usuario/00100sPORTRAIT_00100_BURST20200711152721154_COVER-01.jpg" alt="">
                           <p>@eduardojsc</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bg-red-900 shadow-md rounded-t-md rounded-b-md">
                  <div class="p-5 bg-red-700 rounded-t-md">
                     <div class="bg-black rounded-md shadow-xl">
                        <div class="flex gap-2 pt-4 pl-4">
                           <img src="assets/img/icons/Ellipse 1.svg" alt="">
                           <img src="assets/img/icons/Ellipse 2.svg" alt="">
                           <img src="assets/img/icons/Ellipse 3.svg" alt="">
                        </div>
                        <div class="mt-3 ml-2 javascript">
                           <pre class="whitespace-normal">
                              <code >
                                 const pluckDeep = key => obj => key.split('.').reduce((accum, key) => accum[key], obj)
                                 
                                 const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)
                                 
                                 const unfold = (f, seed) => {
                                    const go = (f, seed, acc) => {
                                       const res = f(seed)
                                       return res ? go(f, res[1], acc.concat([res[0]])) : acc
                                    }
                                    return go(f, seed, [])
                                 }
                              </code>
                           </pre>
                        </div>
                     </div>
                  </div>
                  <div class="p-5 text-white">
                     <h1 class="text-2xl font-bold">Título do projeto</h1>
                     <h2 class="py-5">Essa é a descrição do meu projeto.</h2>
               
                     <div class="grid grid-cols-2">
                        <div class="flex items-center gap-4">
                           <div class="flex gap-2">
                              <img src="assets/img/icons/comments.svg" alt="">
                              <p>13</p>
                           </div>
               
                           <div class="flex gap-2">
                              <img src="assets/img/icons/heart.svg" alt="">
                              <p>13</p>
                           </div>
                        </div>
               
                        <div class="flex items-center justify-end gap-2">
                           <img class="w-8 rounded-full"
                              src="assets/img/usuario/00100sPORTRAIT_00100_BURST20200711152721154_COVER-01.jpg" alt="">
                           <p>@eduardojsc</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bg-yellow-500 shadow-md rounded-t-md rounded-b-md">
                  <div class="p-5 bg-yellow-200 rounded-t-md">
                     <div class="bg-black rounded-md shadow-xl">
                        <div class="flex gap-2 pt-4 pl-4">
                           <img src="assets/img/icons/Ellipse 1.svg" alt="">
                           <img src="assets/img/icons/Ellipse 2.svg" alt="">
                           <img src="assets/img/icons/Ellipse 3.svg" alt="">
                        </div>
                        <div class="mt-3 ml-2 javascript">
                           <pre class="whitespace-normal">
                              <code >
                                 const pluckDeep = key => obj => key.split('.').reduce((accum, key) => accum[key], obj)
                                 
                                 const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)
                                 
                                 const unfold = (f, seed) => {
                                    const go = (f, seed, acc) => {
                                       const res = f(seed)
                                       return res ? go(f, res[1], acc.concat([res[0]])) : acc
                                    }
                                    return go(f, seed, [])
                                 }
                              </code>
                           </pre>
                        </div>
                     </div>
                  </div>
                  <div class="p-5 text-white">
                     <h1 class="text-2xl font-bold">Título do projeto</h1>
                     <h2 class="py-5">Essa é a descrição do meu projeto.</h2>
               
                     <div class="grid grid-cols-2">
                        <div class="flex items-center gap-4">
                           <div class="flex gap-2">
                              <img src="assets/img/icons/comments.svg" alt="">
                              <p>13</p>
                           </div>
               
                           <div class="flex gap-2">
                              <img src="assets/img/icons/heart.svg" alt="">
                              <p>13</p>
                           </div>
                        </div>
               
                        <div class="flex items-center justify-end gap-2">
                           <img class="w-8 rounded-full"
                              src="assets/img/usuario/00100sPORTRAIT_00100_BURST20200711152721154_COVER-01.jpg" alt="">
                           <p>@eduardojsc</p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- FIM CONTEUDO SITE -->
            </div>

         </section>
         
      </main>
   </body>
</html>
