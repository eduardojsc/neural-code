<div x-data="{ hamburgerMenuOpen: false }" class="grid items-center grid-cols-2 antialiased md:grid-cols-layout-2-cols-fr lg:grid-cols-layout-3-cols-fr">

   <!-- LOGO -->
   <div>
      <img class="w-145px" src="assets/img/logo/logo-neuralcode.png" alt="Aqui está a logo da Neural Code">
   </div>

   <!-- CAMPO DE BUSCA DESKTOP -->
   <div class="hidden lg:block">
      <input class="input-text" type="search" placeholder="Busque por algo">
   </div>

   <!-- CAMPO DE BUSCA, LUPA e ICONE HAMBURGUER MENU MOBILE/TABLET -->
   <div class="flex items-center justify-end lg:hidden md:ml-10">

      <div class="hidden w-full md:block">
         <input class="input-text" type="search" placeholder="Busque por algo">
      </div>
      
      <div class="block md:hidden">
         <img src="assets/img/menu/search.svg" alt="Busque códigos aqui!">
      </div>

      <div class="ml-4 ">
         <button 
            class="relative w-12 h-12 text-white rounded-lg focus:outline-none" 
            @click="hamburgerMenuOpen = !hamburgerMenuOpen"
            :class="{
               'hover:bg-default-300 hover:bg-opacity-10 active:bg-opacity-20 focus:bg-opacity-20': hamburgerMenuOpen, 
               ' translate-y-1.5 focus:bg-opacity-0 active:bg-opacity-0': !hamburgerMenuOpen
            }" >
            <div class="absolute block w-5 transform -translate-x-1/2 -translate-y-1/2 left-1/2 top-1/2">
               <span aria-hidden="true"
                  class="block absolute rounded-full h-0.5 w-5 bg-current transform transition duration-500 ease-in-out"
                  :class="{'rotate-45': hamburgerMenuOpen, '-translate-y-1.5 ': !hamburgerMenuOpen }"></span>
               <span aria-hidden="true"
                  class="block absolute rounded-full h-0.5 w-5 bg-current transform transition duration-500 ease-in-out"
                  :class="{'opacity-0': hamburgerMenuOpen } "></span>
               <span aria-hidden="true"
                  class="block absolute rounded-full h-0.5 w-5 bg-current transform transition duration-500 ease-in-out"
                  :class="{'-rotate-45': hamburgerMenuOpen, ' translate-y-1.5': !hamburgerMenuOpen}"></span>
            </div>
         </button>         
      </div>
   </div>
   
   <!-- MENU -->
   <div @click="hamburgerMenuOpen = true" class="flex justify-end col-span-2 lg:hidden">

      <nav  
         class="absolute z-50 h-screen p-4 mt-4 text-white duration-500 transform bg-indigo-900 rounded-lg shadow-lg w-80 lg:transform-none lg:opacity-100 lg:relative"
         :class="{'block ease-in opacity-100' : hamburgerMenuOpen, 'hidden ease-out opacity-0' : !hamburgerMenuOpen}"
      >
         <?php include'assets/includes/menu.php'?>
      </nav>

   </div>

   <!-- USARIO LOGADO -->
   <div class="hidden lg:block">
      <div class="flex justify-end">
         <div class="flex items-center">
            <img class="w-8 rounded-full"
               src="assets/img/usuario/00100sPORTRAIT_00100_BURST20200711152721154_COVER-01.jpg" alt="Imagem de perfil">
            <div class="text-white whitespace-nowrap">
               <p class="px-2">Eduardo</p>
            </div>
         </div>
      </div>
   </div>

</div>