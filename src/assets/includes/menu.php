
<h1 class="hidden tracking-widest lg:block lg:mb-4">Menu</h1>
   
<div class="grid grid-cols-1 gap-4">

   <a href="cod-editor.php">

      <div class="flex items-center gap-3 cursor-pointer hover:opacity-100">
      
         <div class="p-4 bg-default-300 rounded-xl">
            <img src="assets/img/icons/ico-code-maneu.svg" alt="">
         </div>
         <p>Editor de código</p>
      </div>
      
   </a>

   <a href="index.php">
      <div class="flex items-center gap-3 cursor-pointer hover:opacity-100">
      
         <div class="p-4 bg-default-300 rounded-xl">
            <img src="assets/img/icons/comunity.svg" alt="">
         </div>
         <p>Comunidade</p>
      </div>
   </a>

</div>