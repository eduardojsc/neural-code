<!DOCTYPE html>
<html lang="pt-br">

<head>

   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <link rel="shortcut icon" href="assets/img/favicon/favicon.ico" type="image/x-icon">
   <title>Neural Code - De um dev para dev</title>

   <!-- FONTE SITE -->
   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200&display=swap" rel="stylesheet">   
   
   <!-- TAILWIND CSS -->
   <link rel="stylesheet" href="assets/css/tailwind-style.css">

   <!-- ALPINE.JS -->
   <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

   <!-- HIGHLIGHT JS -->
   <link rel="stylesheet" href="assets/js/highlight/styles/default.css">
   <script src="assets/js/highlight/highlight.pack.js"></script>
   <script>hljs.highlightAll();</script>

</head>

<body class="p-5 text-white bg-default-dark font-inter">
      
      <header>
         <?php include_once 'assets/includes/header.php' ?>
      </header>

      <main>
      
         <section class="grid mt-10 lg:grid-cols-layout-3-cols-fr">

            <nav class="hidden lg:block">
               <?php include 'assets/includes/menu.php' ?>
            </nav>

            <div class="grid gap-5 ">
               <!-- CONTEUDO SITE -->
               <div class="p-5 bg-blue-300 rounded-lg">
                  <div class="bg-black rounded-md shadow-xl">
                     <div class="flex gap-2 pt-4 pl-4">
                        <img src="assets/img/icons/Ellipse 1.svg" alt="">
                        <img src="assets/img/icons/Ellipse 2.svg" alt="">
                        <img src="assets/img/icons/Ellipse 3.svg" alt="">
                     </div>
                     <div class="mt-3 ml-2 javascript">
                        <pre class="whitespace-normal lg:whitespace-pre-wrap">
                           <code >
                              const pluckDeep = key => obj => key.split('.').reduce((accum, key) => accum[key], obj)
                              
                              const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)
                              
                              const unfold = (f, seed) => {
                                 const go = (f, seed, acc) => {
                                    const res = f(seed)
                                    return res ? go(f, res[1], acc.concat([res[0]])) : acc
                                 }
                                 return go(f, seed, [])
                              }
                           </code>
                        </pre>
                     </div>
                  </div>
               </div>

               <div class="flex items-center bg-blue-900 rounded-lg shadow-lg">
                  <button class="w-full p-5">
                     Visualizar com o highlight
                  </button>
               </div>
               
               <div class="block lg:hidden">
                  <?php include './assets/includes/cod-editor/form-cod-editor.html'; ?>
               </div>          
               
               <!-- FIM CONTEUDO SITE -->
            </div>
            
            <div class="hidden pl-5 lg:block">
               <?php include './assets/includes/cod-editor/form-cod-editor.html'; ?>
            </div>

         </section>
      
      </main>

      <footer>                   
      </footer>
</body>

</html>