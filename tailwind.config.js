module.exports = {
  purge: [],
  
  darkMode: false, // or 'media' or 'class'
  important: true,

  theme: {
    extend: {

      colors: {

        'default': {
          100: '#96B9FD',
          200: '#7BA4FC',
          300: '#5081FB',
          'dark': '#051D3B',
          400: '#2D415B',
        },

        'color-text': {
          'grey': '#666666'
        }

      },

      fontFamily:{

        'inter': ['"Inter"', 'sans-serif']

      },

      gridTemplateColumns: {

        'layout-3-cols-fr': '1fr 3fr 1fr',
        'layout-2-cols-fr': '1fr 4fr',

      },

      width: {

        '145px': '145px',

      },

      translate:{
        '90%': '90%',
        '50%': '50%',
      }

    },
  },

  variants: {
    extend: {
      backgroundOpacity: ['active'],
      opacity: ['active'],
    },
  },
  
  plugins: [],
}
